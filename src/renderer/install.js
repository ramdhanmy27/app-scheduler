if (!window.__hotData) {
  window.__hotData = {};
  window.__hotDisposeHandlers = {};
}

export default (moduleInstance) => {
  const fileName = moduleInstance.filename;

  moduleInstance.hot = {
    accept() {
      moduleInstance.hot.data = window.__hotData[fileName];

      if (!window.__hotDisposeHandlers[fileName]) {
        window.__hot.push(() => {
          const data = window.__hotData[fileName] = {};
          window.__hotDisposeHandlers[fileName](data);
          require(fileName);
        });

        window.__hotDisposeHandlers[fileName] = () => {};
      }
    },
    dispose(disposeHandler) {
      window.__hotDisposeHandlers[fileName] = disposeHandler;
    },
  };
};
