import _ from "lodash";

export default {
  appendProcessByIndex(processList, index, queue = [], sort = []) {
    return _.sortBy(_.concat(queue, _.filter(processList, { start: index })), sort);
  },
  fifo(process) {
    const queue = _.sortBy(process, ['start', 'id']);
    const schedule = {};
    let counter = 0;

    while (queue.length > 0) {
      // pull the first process item
      const item = Object.assign({}, _.pullAt(queue, 0)[0]);

      if (_.isUndefined(schedule[item.id])) {
        schedule[item.id] = [];
      }

      for (var i = 1; i <= item.size; i++) {
        if (counter < item.start) {
          counter = item.start;
        }

        schedule[item.id].push({
          index: counter++,
          remaining: item.size - i,
          processItem: item,
        });
      }
    }

    return { counter, schedule };
  },
  sjf(process) {
    const processList = _.sortBy(process, ['start', 'id']);
    const schedule = {};
    let counter = 0;

    const lastProcess = processList.length == 0 ? {} : _.last(processList);
    let queue = this.appendProcessByIndex(
      processList,
      counter,
      processList.splice(0, 1),
      ['size', 'start']
    );

    while (queue.length > 0 || counter <= lastProcess.start) {
      if (queue.length == 0) {
        counter++;
        queue = this.appendProcessByIndex(processList, counter, queue, ['size', 'start']);
        continue;
      }

      // pull the first process item
      const item = Object.assign({}, queue.splice(0, 1)[0]);

      if (_.isUndefined(schedule[item.id])) {
        schedule[item.id] = [];
      }

      for (var i = 1; i <= item.size; i++) {
        if (counter < item.start) {
          counter = item.start;
          queue = this.appendProcessByIndex(processList, counter, queue, ['size', 'start']);
        }

        schedule[item.id].push({
          index: counter++,
          remaining: item.size - i,
          processItem: item,
        });

        queue = this.appendProcessByIndex(processList, counter, queue, ['size', 'start']);
      }
    }

    return { counter, schedule };
  },
  srt(process) {
    const processList = _.sortBy(process, ['start', 'id']);
    const schedule = {};
    let counter = 0;

    const lastProcess = processList.length == 0 ? {} : _.last(processList);
    let queue = this.appendProcessByIndex(
      processList,
      counter,
      processList.splice(0, 1),
      ['size', 'start']
    );

    while (queue.length > 0 || counter <= lastProcess.start) {
      if (queue.length == 0) {
        counter++;
        queue = this.appendProcessByIndex(processList, counter, queue, ['size', 'start']);
        continue;
      }

      // pull the first process item
      // const item = Object.assign({}, queue.splice(0, 1)[0]);
      const processItem = queue.splice(0, 1)[0];
      const item = { ...processItem };

      if (_.isUndefined(schedule[item.id])) {
        schedule[item.id] = [];
      }

      for (var i = 1; i <= processItem.size; i++) {
        if (counter < item.start) {
          counter = item.start;
          queue = this.appendProcessByIndex(processList, counter, queue, ['size', 'start']);
        }

        schedule[item.id].push({
          index: counter++,
          remaining: --item.size,
          processItem: processItem,
        });

        queue = this.appendProcessByIndex(processList, counter, queue, ['size', 'start']);
        const firstProcess = _.head(queue);

        if (!!firstProcess && firstProcess.size < item.size) {
          queue.push(item);
          _.sortBy(queue, ['size']);
          break;
        }
      }
    }

    return { counter, schedule };
  },
  rr(process, capacity) {
    let queue = _.sortBy(process, ['start', 'size', 'id']);
    const schedule = {};
    let counter = 0;

    while (queue.length > 0) {
      // pull the first process item
      const processItem = _.pullAt(queue, 0)[0];
      const item = { ...processItem };

      if (_.isUndefined(schedule[item.id])) {
        schedule[item.id] = [];
      }

      for (var i = 1; i <= capacity; i++) {
        if (counter < item.start) {
          counter = item.start;
        }

        schedule[item.id].push({
          index: counter++,
          remaining: --item.size,
          processItem: processItem,
        });

        if (item.size <= 0) {
          break;
        }
      }

      if (item.size > 0) {
        item.start = counter;
        queue.push(item);
        queue = _.sortBy(queue, ['start', 'size', 'id']);
      }

      // infinite loop prevention
      if (counter > 50) {
        break;
      }
    }

    return { counter, schedule };
  },
};
